<?php
class Navigation extends CI_Model {
    public function __construct(){
        parent::__construct();
    }
    public function get_navigation(){
        $data = array(
          'admin' => array(
            //   array(
            //     'url' => 'public/#',
            //     'displayName' => 'Dashboard',
            //     'child' => false,
            //     'heading' => false,
            //     'icon' => 'si-speedometer'
            //   ),
            //   array(
            //     'url' => '#',
            //     'displayName' => 'MASTER',
            //     'child' => false,
            //     'heading' => true,
            //     'icon' => ''
            //   ),
              array(
                'url' => '#',
                'displayName' => 'Master Unit Kerja',
                'child' => array(
                      array(
                        'url' => '/workunit',
                        'displayName' => 'Modul Unit Kerja',
                      ),
                      array(
                        'url' => '#',
                        'displayName' => 'Kategori Unit Kerja',
                      ),
                    ),
                'heading' => false,
                'icon' => 'si-user'
              ),
            ),
        );
        return $data;
    }
}
