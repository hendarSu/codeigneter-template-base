<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Workunit extends CI_Controller{
	private $navigations;
	public function __construct()
	{
		// Call the CI_Controller constructor
		parent::__construct();
		
		// $this->load->model('workunit');
		$this->load->model('navigation');
		$this->navigations = $this->navigation->get_navigation();
		// $session = self::_is_logged_in();
		// if(!$session) redirect('/');

	}

	public function index()
	{
		$data['navigations'] = $this->navigations;
		$this->twig->display('content/workunit', $data);
	}

	public function add()
	{
		// Your Code
	}

	public function update()
	{
		// Your Code
	}

	public function delete()
	{
		// Your Code
	}

	public function get()
	{
		// Your Code
	}

	private function _is_logged_in()
	{
        $user = $this->session->userdata('user_data');
        return isset($user);
	}
	
}
