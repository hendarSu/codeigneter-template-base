<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS");
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	private $navigations;
	public function __construct()
	{
		// Call the CI_Controller constructor
		parent::__construct();
		$this->load->model('navigation');
		$this->navigations = $this->navigation->get_navigation();
	}

	public function index()
	{
		$session = self::_is_logged_in();
		$data['navigations'] = $this->navigations;
		if($session) {
			// Sementara
			$this->twig->display('content/welcome', $data);
		}else {
			$this->twig->display('auth/login');
		}
	}

	private function _is_logged_in()
	{
        $user = $this->session->userdata('user_data');
        return isset($user);
	}
	
}
