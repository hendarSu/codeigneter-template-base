/*
 *  Document   : base_tables_datatables.js
 *  Author     : pixelcave
 *  Description: Custom JS code used in Tables Datatables Page
 */

 var BaseTableDatatables = function() {
    var initDataTable = function() {
      window.tableData = jQuery('.data-workUnit').dataTable({
          order: [[1, 'asc']],
          columnDefs: [{ orderable: false, targets: [0] }],
          pageLength: 10,
          lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
        //   processing: true,
        //   serverSide: true,
        //   ajax: '/workUnit/get',
        //   columns: [
        //           { data: 'id',
        //             render:function(data, type, row) {
        //                 let str = '<div class="checkbox-workUnit">' +
        //                               '<input type="checkbox" name="workUnitId" value="' + data + '" data-toggle="tooltip" title="Select to remove">' +
        //                         '</div>';
        //                 return str;
        //             },
        //           },
        //           { data: 'workUnitcode' },
        //           { data: 'workUnitname'
        //             // render: function(data, type, row) {
        //             //     let str = '<small>'+moment(data).format('DD-MM-YYYY')+'</small>';
        //             //     return str;
        //             // }
        //           },
        //           { data: 'notes',
        //             render: function(data, type, row) {
        //                 let str = '<small>'+data+'</small>';
        //                 return str;
        //             }
        //         },
        //           { data: 'id',
        //             render: function(data, type, row) {
        //                 let str = '<div class="btn-group pull-right"><button type="button" class="btn btn-default btn-sm" name="button" onClick="openModal(\'#modal-update-workUnit\', \'update\', '+data+')"><i class="si si-pencil"></i> Ubah</button></div>';
        //                 return str;
                       
        //             },
        //           },
        //         ],
      }).on('draw.dt', function() {
          $(this).removeAttr('style');
          $('[data-toggle="tooltip"]').tooltip();
          $('.checkbox-workUnit').shiftcheckbox({
              checkboxSelector: ':checkbox',
              selectAll: $('.checkbox-workUnit-all'),
              ignoreClick: 'a',
              onChange: function(checked) {

              },
          });
      });
    };

    // DataTables Bootstrap integration
    var bsDataTables = function() {
      var $DataTable = jQuery.fn.dataTable;

      // Set the defaults for DataTables init
      jQuery.extend(true, $DataTable.defaults, {
          dom:
              '<\'row\'<\'col-sm-6\'l><\'col-sm-6\'f>>' +
              '<\'row\'<\'col-sm-12\'tr>>' +
              '<\'row\'<\'col-sm-6\'i><\'col-sm-6\'p>>',
          renderer: 'bootstrap',
          oLanguage: {
              sLengthMenu: '_MENU_',
              sInfo: 'Showing <strong>_START_</strong>-<strong>_END_</strong> of <strong>_TOTAL_</strong>',
              oPaginate: {
                  sPrevious: '<i class="fa fa-angle-left"></i>',
                  sNext: '<i class="fa fa-angle-right"></i>',
              },
          },
      });

      // Default class modification
      jQuery.extend($DataTable.ext.classes, {
          sWrapper: 'dataTables_wrapper form-inline dt-bootstrap',
          sFilterInput: 'form-control',
          sLengthSelect: 'form-control',
      });

      // Bootstrap paging button renderer
      $DataTable.ext.renderer.pageButton.bootstrap = function(settings, host, idx, buttons, page, pages) {
          var api     = new $DataTable.Api(settings);
          var classes = settings.oClasses;
          var lang    = settings.oLanguage.oPaginate;
          var btnDisplay, btnClass;

          var attach = function(container, buttons) {
              var i, ien, node, button;
              var clickHandler = function(e) {
                  e.preventDefault();
                  if (!jQuery(e.currentTarget).hasClass('disabled')) {
                      api.page(e.data.action).draw(false);
                  }
              };

              for (i = 0, ien = buttons.length; i < ien; i++) {
                  button = buttons[i];

                  if (jQuery.isArray(button)) {
                      attach(container, button);
                  } else {
                      btnDisplay = '';
                      btnClass = '';

                      switch (button) {
                          case 'ellipsis':
                              btnDisplay = '&hellip;';
                              btnClass = 'disabled';
                              break;

                          case 'first':
                              btnDisplay = lang.sFirst;
                              btnClass = button + (page > 0 ? '' : ' disabled');
                              break;

                          case 'previous':
                              btnDisplay = lang.sPrevious;
                              btnClass = button + (page > 0 ? '' : ' disabled');
                              break;

                          case 'next':
                              btnDisplay = lang.sNext;
                              btnClass = button + (page < pages - 1 ? '' : ' disabled');
                              break;

                          case 'last':
                              btnDisplay = lang.sLast;
                              btnClass = button + (page < pages - 1 ? '' : ' disabled');
                              break;

                          default:
                              btnDisplay = button + 1;
                              btnClass = page === button ?
                                      'active' : '';
                              break;
                      }

                      if (btnDisplay) {
                          node = jQuery('<li>', {
                              class: classes.sPageButton + ' ' + btnClass,
                              'aria-controls': settings.sTableId,
                              tabindex: settings.iTabIndex,
                              id: idx === 0 && typeof button === 'string' ?
                                      settings.sTableId + '_' + button :
                                      null,
                          })
                          .append(jQuery('<a>', {
                                  href: '#',
                              })
                              .html(btnDisplay)
                          )
                          .appendTo(container);

                          settings.oApi._fnBindAction(
                              node, {action: button}, clickHandler
                          );
                      }
                  }
              }
          };

          attach(
              jQuery(host).empty().html('<ul class="pagination"/>').children('ul'),
              buttons
          );
      };

      // TableTools Bootstrap compatibility - Required TableTools 2.1+
      if ($DataTable.TableTools) {
          // Set the classes that TableTools uses to something suitable for Bootstrap
          jQuery.extend(true, $DataTable.TableTools.classes, {
              container: 'DTTT btn-group',
              buttons: {
                  normal: 'btn btn-default',
                  disabled: 'disabled',
              },
              collection: {
                  container: 'DTTT_dropdown dropdown-menu',
                  buttons: {
                      normal: '',
                      disabled: 'disabled',
                  },
              },
              print: {
                  info: 'DTTT_print_info',
              },
              select: {
                  row: 'active',
              },
          });

          // Have the collection use a bootstrap compatible drop down
          jQuery.extend(true, $DataTable.TableTools.DEFAULTS.oTags, {
              collection: {
                  container: 'ul',
                  button: 'li',
                  liner: 'a',
              },
          });
      }
  };

    var initValidationAdd = function() {
      jQuery('.form-add-workUnit').validate({
          errorClass: 'help-block text-right animated fadeInDown',
          errorElement: 'div',
          errorPlacement: function(error, e) {
              jQuery(e).parents('.form-group > div').append(error);
          },

          highlight: function(e) {
              jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
              jQuery(e).closest('.help-block').remove();
          },

          success: function(e) {
              jQuery(e).closest('.form-group').removeClass('has-error');
              jQuery(e).closest('.help-block').remove();
          },

          rules: {
              rep_name: {
                  required: true,
              },
              rep_description: {
                  required: true,
              },

          },
          messages: {
              rep_name: {
                  required: 'Masukkan Nama Jabatan',
              },
              rep_description: {
                  required: "Masukkan Keterangan",
              },
          },
          submitHandler: function(form) {
              $form = $(form);
              var button = $form.find('button[type="submit"]');
              button.attr('disabled', 'disabled');
              button.text('menyimpan..');
              $.ajax({
                  url:$form.attr('action'),
                  type:'POST',
                  data:$form.serialize(),
                  success: function(res) {
                      // res = $.parseJSON(res);
                      // alert(res.data);
                      if (res.data == '1') {
                          form.reset();
                          swal({
                            title: "",
                            text: "Data berhasil disimpan" ,
                            type: "success",
                            // showCancelButton: true,
                            confirmButtonColor: "#3498DB",
                            // confirmButtonText: "Lanjut ke Menu Utama",
                            // cancelButtonText: "Tetap Disini",
                            closeOnConfirm: true
                          }).then((result) => {
                            if (result.value) {
                                $('#modal-add-workUnit').modal('hide');
                                window.tableData.api().ajax.reload();
                            }
                          });
                      }else{
                          swal("Oops...", res.data, "error");
                      }
                      button.removeAttr('disabled');
                      button.text('Simpan');
                  },

                  error: function(jqXHR, exception) {
                      // alert();
                      swal("Oops...", jqXHR.status, "error");

                      console.log(jqXHR);
                      button.removeAttr('disabled');
                      button.text('Save');
                  },
              });
              return false; // required to block normal submit since you used ajax
          },
      });
  };

    var initValidationUpdate = function() {
        jQuery('.form-update-workUnit').validate({
            errorClass: 'help-block text-right animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group > div').append(error);
            },

            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },

            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },

            rules: {
                rep_name: {
                    required: true,
                },
                rep_description: {
                    required: true,
                },

            },
            messages: {
                rep_name: {
                    required: 'Masukkan Nama Jabatan',
                },
                rep_description: {
                    required: "Masukkan Keterangan",
                },
            },
            submitHandler: function(form) {
                $form = $(form);
                var button = $form.find('button[type="submit"]');
                button.attr('disabled', 'disabled');
                button.text('menyimpan..');
                $.ajax({
                    url:$form.attr('action'),
                    type:'POST',
                    data:$form.serialize(),
                    success: function(res) {
                        // res = $.parseJSON(res);
                        // alert(res.data);
                        if (res.data == '1') {
                            form.reset();
                            swal({
                              title: "",
                              text: "Data berhasil disimpan" ,
                              type: "success",
                              // showCancelButton: true,
                              confirmButtonColor: "#3498DB",
                              // confirmButtonText: "Lanjut ke Menu Utama",
                              // cancelButtonText: "Tetap Disini",
                              closeOnConfirm: true
                            }).then((result) => {
                                if (result.value) {
                                    $('#modal-update-workUnit').modal('hide');
                                    window.tableData.api().ajax.reload();
                                }
                              });
                        }else{
                            swal("Oops...", res.data, "error");
                        }
                        button.removeAttr('disabled');
                        button.text('Simpan');
                    },

                    error: function(jqXHR, exception) {
                        // alert();
                        swal("Oops...", jqXHR.status, "error");

                        console.log(jqXHR);
                        button.removeAttr('disabled');
                        button.text('Save');
                    },
                });
                return false; // required to block normal submit since you used ajax
            },
        });
    };

    return {
        init: function() {
            initValidationAdd();
            initValidationUpdate();
            bsDataTables();
            initDataTable();
        },
    };
}();

// Initialize when page loads
jQuery(function() {
    BaseTableDatatables.init();
});

/* PROCCESSING */
// open modal
function openModal(target, type, id) {
    if (type == 'update') {
        $.get('/workUnit/get/' + id, function(res) {
            let workUnit = res.data;                
            $('.form-update-workUnit').attr('action', '/workUnit/update/' + id);
        });
    }

    $(target).modal('show');
}
// remove
$(document).on('change', '.data-workUnit input:checkbox', function() {
    if ($('.data-workUnit input:checkbox:checked').length > 0) {
        $('.btn-delete-workUnit').removeAttr('disabled');
    }else {
        $('.btn-delete-workUnit').attr('disabled', 'disabled');
    }
});
$('.btn-delete-workUnit').click(function(e) {
    if ($('.data-workUnit input:checkbox:checked').length > 0) {
        var conf = confirm('Hapus data yang dipilih?');
        if (conf) {
            var data = $('.data-workUnit input:checkbox:checked').serialize();

            $.ajax({
                url:'/workUnit/remove',
                type:'POST',
                data:data,
                success: function(res) {
                    if (res.data == '1') {
                        swal({
                          title: "",
                          text: "Data berhasil disimpan" ,
                          type: "success",
                          // showCancelButton: true,
                          confirmButtonColor: "#3498DB",
                          // confirmButtonText: "Lanjut ke Menu Utama",
                          // cancelButtonText: "Tetap Disini",
                          closeOnConfirm: true
                        }).then((result) => {
                            if (result.value) {
                                window.tableData.api().ajax.reload();
                                $('.btn-delete-workUnit').attr('disabled', 'disabled');
                                $('.data-workUnit input:checkbox:checked').removeAttr('checked');
                            }
                          });
                    }else {
                        swal("Oops...", res.data, "error");
                    }
                },

                error: function(jqXHR, exception) {
                    swal("Oops...", jqXHR.status, "error");
                },
            });
        }
    }else {
        alert('Minimal pilih 1 data untuk dihapus!');
    }
});